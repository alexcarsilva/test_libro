import { DiscoveryMovie } from "./discoveryMovie";

export interface Discovery {
    page: number,
    results: DiscoveryMovie[],
    total_pages: number,
    total_results: number
}