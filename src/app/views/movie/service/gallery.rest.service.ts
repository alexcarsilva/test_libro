import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Movie } from 'src/app/models/movie';

@Injectable({
  providedIn: 'root'
})
export class MovieRestService {

  url = 'https://api.themoviedb.org/3/movie/';

  constructor(private httpClient: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  public getMovie(id: number): Observable<Movie> {
    return this.httpClient.get<Movie>(
      this.url + `${id}?api_key=c7ed75aef6fbcfe8f4f565b6815d9f2d&language=pt-BR&append_to_response=credits`
    );
  }
}
