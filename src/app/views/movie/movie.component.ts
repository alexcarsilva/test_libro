import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Movie } from 'src/app/models/movie';
import { __param } from 'tslib';
import { MovieRestService } from './service/gallery.rest.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {
  public movie: Movie;
  public genres: string;
  public directorTitle: string;
  public director: string;
  public crews: any[];
  public releaseDate: string;
  public id: number = 0;
  public innerWidth: number = window.innerWidth;

  constructor(private MovieRestService: MovieRestService,
    private route: ActivatedRoute) {
    this.id = parseInt(this.route.snapshot.paramMap.get('id'));
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }

  ngOnInit() {
    this.MovieRestService.getMovie(this.id).subscribe((movie: Movie) => {
      this.movie = movie;
      this.genres = movie.genres.map(genre => genre.name).join(', ');
      const directorTemp = movie.credits.crew.filter(crew => crew.job === 'Director');
      this.releaseDate = moment(movie.release_date).format("DD/MM/YYYY");
      if (directorTemp.length > 1) {
        this.directorTitle = 'Diretores: ';
        this.director = directorTemp.map(director => director.name).join(', ');
      } else if (directorTemp.length > 0) {
        this.directorTitle = 'Diretor: ';
        this.director = directorTemp[0].name;
      }
      this.crews = _.orderBy(movie.credits.crew, crew => crew.job);
    });
  }
}
