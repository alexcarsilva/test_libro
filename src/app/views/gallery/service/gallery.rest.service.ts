import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Discovery } from 'src/app/models/discovery';
import { Movie } from 'src/app/models/movie';

@Injectable({
  providedIn: 'root'
})
export class GalleryRestService {
  constructor(private httpClient: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  public getLastMovies(): Observable<Discovery> {
    return this.httpClient.get<Discovery>(
      'https://api.themoviedb.org/3/trending/movie/day?api_key=c7ed75aef6fbcfe8f4f565b6815d9f2d&language=pt'
    );
  }

  public getTopMovies(): Observable<Discovery> {
    return this.httpClient.get<Discovery>(
      'https://api.themoviedb.org/3/movie/top_rated?api_key=c7ed75aef6fbcfe8f4f565b6815d9f2d&language=pt'
    );
  }

  public getSearchMovies(search: string, page: number): Observable<Discovery> {
    return this.httpClient.get<Discovery>(
      `https://api.themoviedb.org/3/search/movie?api_key=c7ed75aef6fbcfe8f4f565b6815d9f2d&query=${search}&page=${page}`
    );
  }
}
