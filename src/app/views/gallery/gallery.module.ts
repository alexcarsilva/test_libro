
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GalleryComponent } from './gallery.component';
import { MatInputModule } from '@angular/material/input'

@NgModule({
  declarations: [
    GalleryComponent,
  ],
  imports: [
    CommonModule,
    MatInputModule
  ],
  exports: [
    GalleryComponent
  ]
})

export class GalleryModule { }
