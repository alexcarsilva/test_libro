import { Component, OnInit } from '@angular/core';
import { Discovery } from 'src/app/models/discovery';
import { DiscoveryMovie } from 'src/app/models/discoveryMovie';
import { Movie } from 'src/app/models/movie';
import { GalleryRestService } from './service/gallery.rest.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
  public movies: DiscoveryMovie[] = [];
  public topMovies: DiscoveryMovie[] = [];
  public moviesSearch: DiscoveryMovie[] = [];
  public search: string;
  public searchInput: string;
  public pageTotal: number = 0;
  public page: number = 1;

  constructor(private galleryRestService: GalleryRestService) {

  }

  ngOnInit() {
    this.galleryRestService.getLastMovies().subscribe((discovery) => {
      this.movies = discovery.results;
    });

    this.galleryRestService.getTopMovies().subscribe((discovery) => {
      this.topMovies = discovery.results;
    });
  }

  onSearch() {
    if (this.searchInput != null && this.searchInput.trim() != '') {
      this.galleryRestService.getSearchMovies(this.searchInput, 1).subscribe((discovery) => {
        console.log(discovery)
        this.search = this.searchInput;
        this.moviesSearch = discovery.results;
        this.pageTotal = discovery.total_results
        this.page = 1;
      });
    } else {
      this.search = undefined;
      this.moviesSearch = [];
      this.pageTotal = 0;
    }
  }

  changePage(page: any) {
    this.galleryRestService.getSearchMovies(this.search, page.pageIndex + 1).subscribe((discovery) => {
      this.moviesSearch = discovery.results;
      this.pageTotal = discovery.total_results
      this.page = page.pageIndex;
    });
  }
}
