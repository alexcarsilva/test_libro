import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MovieComponent } from './views/movie/movie.component';
import { GalleryComponent } from './views/gallery/gallery.component';


const routes: Routes = [
  {
    path: 'gallery',
    component: GalleryComponent
  },
  {
    path: 'movie/:id',
    component: MovieComponent
  },
  {
    path: '',
    redirectTo: "gallery",
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: "gallery"
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
